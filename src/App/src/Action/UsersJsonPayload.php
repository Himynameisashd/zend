<?php

namespace App\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;

class UsersJsonPayload implements ServerMiddlewareInterface
{
    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {   
        $request->withHeader('Content-Type', 'application/json');
        
        $json = array (
            'ID' => '39CA2939-38C0-4C4E-AE6C-CFA5172B8CEB',
            'name' => 'Doe',
            'first-name' => 'John',
            'age' => 25,
            'hobbies' => 
            array (
              0 => 'reading',
              1 => 'cinema',
              2 => 
              array (
                'sports' => 
                array (
                  0 => 'volley-ball',
                  1 => 'snowboard',
                ),
              ),
            ),
            'address' => 
            array (
            ),
          );
        
        return new JsonResponse($json);
    }
}
