<?php

namespace App\Action;

use Interop\Http\ServerMiddleware\DelegateInterface;
use Interop\Http\ServerMiddleware\MiddlewareInterface as ServerMiddlewareInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;


class UsersAction implements ServerMiddlewareInterface
{
    private $router;

    private $template;

    public function __construct(Router\RouterInterface $router, Template\TemplateRendererInterface $template = null)
    {  
        $this->router   = $router;
        $this->template = $template;
    }

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        
            $data = array (
                array(
                    'ID' => '1',
                    'name' => 'Dave',
                    'surname' => 'Jones',
                    'email' => 'test@test.com'
                ),
                 array(
                    'ID' => '2',
                    'name' => 'Pete',
                    'surname' => 'Davies',
                    'email' => 'pete@test.com'
                )              
          );
        return new HtmlResponse($this->template->render('users::payload', array('users'=>$data)));
    }
}
