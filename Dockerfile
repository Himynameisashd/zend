FROM php:7-apache

RUN apt-get update -y && apt-get install -y libpng-dev curl libcurl4-openssl-dev libzip-dev

RUN apt-get install vim -y

RUN docker-php-ext-install pdo pdo_mysql gd curl mysqli zip

RUN pecl install xdebug \
    //&& docker-php-ext-enable xdebug
#RUN echo 'zend_extension="/usr/local/lib/php/extensions/no-debug-non-zts-20151012/xdebug.so"' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.enable=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_port=9000' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_enable=1' >> /usr/local/etc/php/php.ini
#RUN echo 'xdebug.remote_connect_back=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_handler=dbgp' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_mode=req' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_autostart=true' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_host=192.168.1.14' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.var_display_max_data= -1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.idekey=netbeans-xdebug' >> /usr/local/etc/php/php.ini


RUN a2enmod rewrite
RUN service apache2 restart

COPY sites-available/000-default.conf /etc/apache2/sites-available

RUN service apache2 restart
